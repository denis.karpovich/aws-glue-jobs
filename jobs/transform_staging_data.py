import sys

from awsglue.job import Job
from awsglue.dynamicframe import DynamicFrame
from awsglue.transforms import Filter, ResolveChoice
from awsglue.context import GlueContext
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from pyspark.sql.functions import col, trim, struct, concat_ws

SUPPORTED_LANGUAGES = {
    "ar", "it", "ko", "th", "ja", "en", "de", "es","pt",
    "tr", "ru", "zh", "fa", "fr", "ur", "nl", "hi", "vi", "id"
}

args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "s3_output_path",
    "source_glue_database",
    "source_glue_table"
])
s3_output_path = args["s3_output_path"]
source_glue_database = args["source_glue_database"]
source_glue_table = args["source_glue_table"]

glue_context = GlueContext(SparkContext.getOrCreate())
job = Job(glue_context)
job.init(args["JOB_NAME"], args)

dynamic_frame = glue_context.create_dynamic_frame.from_catalog(
    database=source_glue_database,
    table_name=source_glue_table,
)
resolved_dynamic_frame = ResolveChoice.apply(
    dynamic_frame, specs=[("id", "cast:long")]
)
filtered_dynamic_frame = Filter.apply(
    resolved_dynamic_frame,
    f=lambda x: x["lang"] in SUPPORTED_LANGUAGES
)
dataframe = filtered_dynamic_frame.toDF()
if dataframe.count() > 0:
    prepared_dataframe = dataframe.withColumn(
        "features",
        struct([
            trim(concat_ws(" ", "title", "content")).alias("text")
        ])
    ).drop("title", "content")
    datasink_dynamic_frame = DynamicFrame.fromDF(
        prepared_dataframe, glue_context, "datasink_dynamic_frame"
    )
    glue_context.write_dynamic_frame.from_options(
        frame=datasink_dynamic_frame,
        connection_type="s3",
        connection_options={
            "path": s3_output_path,
            "partitionKeys": ["lang", "hostname", "store"],
        },
        format="json",
    )
job.commit()
