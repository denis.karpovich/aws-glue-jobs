import sys

from awsglue.job import Job
from awsglue.context import GlueContext
from awsglue.transforms import RenameField, ResolveChoice
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext

args = getResolvedOptions(sys.argv, [
    "JOB_NAME",
    "s3_output_path",
    "source_glue_database",
    "source_glue_table"
])
s3_output_path = args["s3_output_path"]
source_glue_database = args["source_glue_database"]
source_glue_table = args["source_glue_table"]

glue_context = GlueContext(SparkContext.getOrCreate())
job = Job(glue_context)
job.init(args["JOB_NAME"], args)

dynamic_frame = glue_context.create_dynamic_frame.from_catalog(
    database=source_glue_database,
    table_name=source_glue_table,
)
resolved_dynamic_frame = ResolveChoice.apply(
    dynamic_frame, specs=[("id", "cast:long")]
)
flattened_dynamic_frame = dynamic_frame.apply_mapping([
    ("id", "Long", "id", "Long"),
    ("store", "String", "store", "String"),
    ("hostname", "String", "hostname", "String"),
    ("SageMakerOutput.lemma", "String", "tokens", "String")
])
final_dynamic_frame = flattened_dynamic_frame.coalesce(5)
glue_context.write_dynamic_frame.from_options(
    frame=final_dynamic_frame,
    connection_type="s3",
    connection_options={
        "path": s3_output_path,
        "partitionKeys": ["hostname", "store"],
    },
    format="csv",
    format_options={
        "writeHeader": False,
    },
)
job.commit()
