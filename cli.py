import hashlib
import pathlib
import shutil
from typing import List, Dict

import boto3
import click

AWS_REGION_NAME = "us-east-2"
AWS_PROFILE_NAME = "appfollow"
AWS_S3_BUCKET = "reviews-lemmatization-glue-scripts"

ROOT_DIR = pathlib.Path(__file__).parent
JOBS_DIR = ROOT_DIR / "jobs"
TEMPLATES_DIR = ROOT_DIR / "templates"


def get_boto3_session() -> boto3.Session:
    session = boto3.Session(profile_name=AWS_PROFILE_NAME)
    return session


def get_s3_client():
    session = get_boto3_session()
    s3_client = session.client(service_name="s3", region_name=AWS_REGION_NAME)
    return s3_client


def get_glue_client():
    session = get_boto3_session()
    glue_client = session.client(service_name="glue", region_name=AWS_REGION_NAME)
    return glue_client


def list_jobs_scripts() -> List[pathlib.PosixPath]:
    jobs_scripts = list(pathlib.Path("jobs").iterdir())
    return jobs_scripts


def get_templates() -> Dict[str, pathlib.Path]:
    return {filepath.stem: filepath for filepath in TEMPLATES_DIR.glob("*")}


def copy_job_script_template(job_script_name: str, template_filepath: pathlib.Path):
    job_script_filepath = JOBS_DIR / job_script_name
    if job_script_filepath.exists():
        raise ValueError(f"Job script with name {job_script_name} already exists.")
    shutil.copyfile(
        src=template_filepath,
        dst=job_script_filepath,
    )


@click.group()
def cli():
    pass


@cli.command()
def list_glue_jobs():
    glue_client = get_glue_client()
    response = glue_client.list_jobs()
    jobs = response["JobNames"]
    if not jobs:
        click.echo("There are no jobs")
    else:
        for job in jobs:
            click.echo(job)


@cli.command()
def delete_glue_jobs():
    glue_client = get_glue_client()
    jobs_scripts = list_jobs_scripts()
    click.echo("Start deleting all jobs defined in the AWS Glue")
    for script in jobs_scripts:
        job_name = script.stem.replace("_", "-")
        click.echo(f"Deleting {job_name!r} job")
        glue_client.delete_job(JobName=job_name)
    click.echo("Done")


def load_manifest(s3_client) -> Dict:
    manifest = dict()
    paginator = s3_client.get_paginator("list_objects_v2")
    for page in paginator.paginate(Bucket=AWS_S3_BUCKET):
        for obj in page.get("Contents", []):
            manifest[obj["Key"]] = obj["ETag"]
    return manifest


def remove_keys(s3_client, keys):
    to_delete = {"Objects": []}
    for key in keys:
        to_delete["Objects"].append({
            "Key": key
        })
    return s3_client.delete_objects(Bucket=AWS_S3_BUCKET, Delete=to_delete)



def gen_etag(file_path, chunk_size=8 * 1024 * 1024):
    md5s = []

    with open(file_path, 'rb') as fp:
        while True:
            data = fp.read(chunk_size)
            if not data:
                break
            md5s.append(hashlib.md5(data))

    if len(md5s) < 1:
        return '"{}"'.format(hashlib.md5().hexdigest())

    if len(md5s) == 1:
        return '"{}"'.format(md5s[0].hexdigest())

    digests = b''.join(m.digest() for m in md5s)
    digests_md5 = hashlib.md5(digests)
    return '"{}-{}"'.format(digests_md5.hexdigest(), len(md5s))


def upload_file(s3_client, filepath: pathlib.Path, key: str, manifest: Dict):
    etag = gen_etag(filepath)
    if manifest.get(key, "") == etag:
        click.echo(f"Skipping {filepath}, etags match")
        return
    click.echo(f"Uploading {filepath}")
    return s3_client.upload_file(str(filepath), AWS_S3_BUCKET, key)


@cli.command()
@click.option("--delete", is_flag=True)
def sync_jobs_scripts(delete: bool):
    s3_client = get_s3_client()
    manifest = load_manifest(s3_client)
    keys_to_remove = list(manifest.keys())
    jobs_scripts = list_jobs_scripts()
    click.echo("Start updating jobs scripts stored on the AWS S3")
    for script in jobs_scripts:
        upload_file(s3_client, script, script.name, manifest)
        if script.name in keys_to_remove:
            keys_to_remove.remove(script.name)
    if delete and keys_to_remove:
        remove_keys(s3_client, keys_to_remove)
    click.echo("Done")


@cli.command()
@click.option("--job-script-name", required=True)
@click.option("--template", default="default")
def create_job_script(job_script_name: str, template: str):
    if not job_script_name.endswith(".py"):
        job_script_name += ".py"
    valid_templates = get_templates()
    if template not in valid_templates:
        raise click.UsageError(
            f"Unrecognized template name {template}.  Valid "
            f"template names: {', '.join(valid_templates.keys())}"
        )
    template_filepath = valid_templates[template]
    try:
        copy_job_script_template(job_script_name, template_filepath)
    except ValueError as exc:
        raise click.UsageError(exc)
    click.echo(
        f"New job script {job_script_name} successfully "
        f"created from template {template}"
    )


if __name__ == "__main__":
    cli()
